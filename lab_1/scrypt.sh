#!/bin/bash

echo -e "============================"
echo -e "Check file inode"
echo -e "----------------------------"
echo -e "???? Name Surname, group ?????\n\n"
echo -e "DESCRIPTION:"
echo -e "Determines if the file's inode has" 
echo -e "  changed since growth retardation"
echo -e "============================\n"

PROGRUN=true

while $PROGRUN; do
    # Вывод текущей директории
    echo -e "The current directory is:\t `pwd`\n"

    # Запрос имени файла
    read -p "Enter the file path: " FILENAME
    if ! [ -f "$FILENAME" ]; then 
        echo -e "ERROR: File not found\n"
        continue
    fi

    # Получение даты изменения дискриптора файла
    CHANGE_FILE_DATE=`stat --format=%z $FILENAME`
    # Нормализация даты
    NORM_CHANGE_FILE_DATE=`date --date="$FFF" --rfc-3339=seconds`
    
    # Запрос проверочный даты
    read -p "Enter date [YY-MM-DD HH-mm-ss]: " INPUT_DATE
    # Нормализация проверочный даты
    NORM_INPUT_DATE=`date --date=$INPUT_DATE --rfc-3339=seconds`

    echo ""
    echo -e "Input date: $NORM_INPUT_DATE"
    echo -e "Change date: $NORM_CHANGE_FILE_DATE\n"

    # Проверка дат между собой
    if [ "$NORM_CHANGE_FILE_DATE" \< "$NORM_INPUT_DATE" ]; then
        echo -e "File inode NOT changed after specified date" 
    else
        echo -e "File inode changed after specified date"
        exit 120
    fi

    # Повторить ли выполнение программы
	read -p "Retry? [y/n]: " SELECT
	if ! [ "$SELECT" = "y" ]; then
        	PROGRUN=false
    fi

done
